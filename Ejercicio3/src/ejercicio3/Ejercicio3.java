package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
// Crear un proyecto en Eclipse llamado Ejercicio3 que solicite una cadena
//y la cambie a minúsculas. Subirlo a repositorio remoto. (tercer commit)

		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un texto");
		String texto = input.nextLine().toLowerCase();
		System.out.println(texto);
				
		input.close();
	}

}
